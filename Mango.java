package bcas.inter.java;

public class Mango implements Fruits, Madein {

		@Override
		public void printFruitName() {
			System.out.println("This is a mango");

		}

		@Override
		public String FruitColor() {

			return "Yellow";
		}

		@Override
		public String shape() {

			return "Big";
		}

		@Override
		public String[] country() {
			String[] country = { "India", "Australia" };
			return country;
		}

	}
