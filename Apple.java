package bcas.inter.java;

	public class Apple implements Fruits, Madein {

		@Override
		public void printFruitName() {
			System.out.println("This is an Apple");

		}

		@Override
		public String FruitColor() {

			return "Red";
		}

		@Override
		public String shape() {

			return "Small";
		}

		@Override
		public String[] country() {
			String[] country = { "India", "Australia" };
			return country;
		}
}
