package bcas.inter.java;

	public class FruitDemo {

		public static void main(String[] args) {

			Mango mango = new Mango();
			Apple apple = new Apple();

			System.out.println(apple.FruitColor());
			System.out.println(mango.FruitColor());

			mango.printFruitName();
			apple.printFruitName();

			System.out.println(apple.shape());
			System.out.println(mango.shape());

			String[] country = apple.country();
			for (String made : country) {
				System.out.print(made + ",");

			}
		}
	}
